<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Document;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DocumentController extends Controller
{
    public function index()
    {
        $documents = Document::all();
        return view('documents.index', compact('documents'));
    }

    public function create()
    {
        return view('documents.create');
    }

    public function get()
    {
        return view('documents.get');
    }

    public function info()
    {
        return view('documents.getInfo');
    }

    public function remove()
    {
        return view('documents.remove');
    }

    public function store(Request $request)
    {
        $request->validate([
            'uploaded_file' => 'required|file|mimes:pdf',
        ]);

        if ($request->hasFile('uploaded_file')) {
            $uploadedFile = $request->file('uploaded_file');
            $title = $uploadedFile->getClientOriginalName();

            if (Storage::exists('public/uploads/' . $title) || Document::where('title', $title)->exists()) {
                return redirect()->route('documents.create')->with('error', 'There already is a document with that name.');
            }

            $path = Storage::putFileAs('public/uploads', $uploadedFile, $uploadedFile->getClientOriginalName());

            Document::create(['title' => $title]);

            return redirect()->route('documents.create')->with('success', 'Document uploaded successfully.');
        }

        return redirect()->route('documents.create')->with('error', 'File upload failed.');
    }

    public function show(Request $request)
    {
        $request->validate([
            'filename' => 'required',
        ]);

        $filename = $request->input('filename') . '.pdf';

        if ($filename) {

            $filePath = 'public/uploads/' . $filename;

            if (Storage::exists($filePath)) {
                return Storage::download($filePath, $filename);
            } else {
                return redirect()->route('documents.get')->with('error', 'File not found.');
            }
        }
        
        return redirect()->route('documents.get')->with('error', 'File not found.');
    }

    public function delete(Request $request)
    {
        $request->validate([
            'filename' => 'required',
        ]);

        $filename = $request->input('filename') . '.pdf';
        $filepath = 'uploads/' . $filename;
    
        if ($filename && Storage::disk('public')->exists($filepath)) {
            Storage::disk('public')->delete($filepath);

            $dbItem = Document::where('title', $filename);
            if ($dbItem) {
                $dbItem->delete();
                return redirect()->route('documents.remove')->with('success', 'File deleted successfully.');
            } else {
                return redirect()->route('documents.remove')->with('error', 'File deleted, but could not find Database entry.');
            }

            Document::delete();
            return redirect()->route('documents.remove')->with('success', 'File deleted successfully.');
        }
        
        return redirect()->route('documents.remove')->with('error', 'File not found.');
    }

    public function getAllFilenames() {
        $folderPath = 'public/uploads';

        $files = Storage::files($folderPath);

        $filenames = array_map('basename', $files);

        return $filenames;
    }

    public function getFileInformation(Request $request)
    {
        $request->validate([
            'filename' => 'required',
        ]);

        $filename = $request->input('filename') . '.pdf';

        $document = Document::where('title', $filename)->first();

        return $document;
    }
}
