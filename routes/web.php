<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DocumentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Actual User Routes
Route::get('/', function () {
    return view('welcome');
});

Route::get('/documents/index', [DocumentController::class, 'index'])->name('documents.index'); // unused
Route::get('/documents/create', [DocumentController::class, 'create'])->name('documents.create');
Route::get('/documents/get', [DocumentController::class, 'get'])->name('documents.get');
Route::get('/documents/all', [DocumentController::class, 'getAllFilenames'])->name('documents.getAllFilenames');
Route::get('/documents/info', [DocumentController::class, 'info'])->name('documents.info');
Route::get('/documents/remove', [DocumentController::class, 'remove'])->name('documents.remove');

// API
Route::post('/documents', [DocumentController::class, 'store'])->name('documents.store');
Route::get('/documents/fileInformation', [DocumentController::class, 'getFileInformation'])->name('documents.getFileInformation');
Route::get('/documents', [DocumentController::class, 'show'])->name('documents.show');
Route::get('/documents/delete', [DocumentController::class, 'delete'])->name('documents.delete');