# Laravel PDF DB

PDF-Document Storage Server (storing as file and information in database) made with Laravel and MySQL.

## Setting up

> You need to have PHP and Composer installed.

### PHP Extensions
#### `php_fileinfo`
Ensure that `php_fileinfo` is enabled: locate your `php.ini` and make sure the following is not commented out:
```ini
extension=php_fileinfo
```

#### `mysqli`
Ensure that `mysqli` is enabled: locate your `php.ini` and make sure the following is not commented out:
```ini
extension=mysqli
```

### MySQL & `.env`
Set up MySQL and configure your `.env` file with the needed information to access your database.

### Running
Run the following code in your terminal inside the directory of this project.

#### Installing dependencies
```bash
composer install
```

#### Initiate Database
```bash
php artisan migrate
```

#### Serve
```bash
php artisan serve
```

> Your server should now be live.