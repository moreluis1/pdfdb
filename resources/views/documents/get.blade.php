<!DOCTYPE html>
<html>
<head>
    <title>Retrieve a Document</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    <style>html { font-family:Figtree, sans-serif; } a { color: black; }</style>
</head>
<body>
    <div>
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        
        @if (session('error'))
            <div class="alert alert-error">
                {{ session('error') }}
            </div>
        @endif 
    </div>
    <div>
        <a href="/" style="display: inline-block">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-bar-left" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M12.5 15a.5.5 0 0 1-.5-.5v-13a.5.5 0 0 1 1 0v13a.5.5 0 0 1-.5.5ZM10 8a.5.5 0 0 1-.5.5H3.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L3.707 7.5H9.5a.5.5 0 0 1 .5.5Z"/>
            </svg> 
            Back to Start
        </a>

        <h2>Get a Document</h2>

        <form action="{{ route('documents.show') }}" method="get" enctype="multipart/form-data">
            @csrf
            <label for="filename">Enter PDF File name:</label>
            <div style="display: flex"> <input type="text" name="filename" id="filename" required> <p>.pdf</p> </div>
            <br>
            <button type="submit">Upload</button>
        </form>
    </div>
</body>
</html>
